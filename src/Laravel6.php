<?php

namespace Simplexi\Laravel6;

class Laravel6
{
    public function greet(String $sName)
    {
        return 'Hi ' . $sName . '! How are you doing today?';
    }
}
